#!/bin/python

import util.text
import util.generator
import pyglet
import ui.ui
import util.config
from util.controls import KeyboardDispatcher
from util.config import ConfigManager, KeymapConfigManager

try:
    pyglet.lib.load_library('avbin')
    pyglet.have_avbin = True
    MP3_SUPPORT = True
except ImportError:
    MP3_SUPPORT = False

PROJECT_VERSION = "v0.0.1"
PROJECT_NAME = "Blind-Space Python {}".format(PROJECT_VERSION)
CONFIG_LANG = "fr"

translator = util.text.TextParser('./data/text/', CONFIG_LANG)
audio_generator = util.generator.GoogleTTSGenerator()

sound_file_manager = util.generator.AudiofileManager(CONFIG_LANG, './data/sounds/generated')
sound_file_manager.set_generator(audio_generator)
sound_file_manager.set_key_list(translator.get_keys())
sound_file_manager.set_parser(translator)
sound_file_manager.build_audio_files()
if not MP3_SUPPORT:
    sound_file_manager.convert_audio_files(source_format="mp3", dest_format="wav")


# ==========
# TMP
# ==========
kb_handler = KeyboardDispatcher()
config = ConfigManager()
if not config.check_audio_drivers():
    print "Audio driver OpenAL is missing !"

pyglet.font.add_file('./data/fonts/Computerfont.ttf')
pyglet.font.add_file('./data/fonts/DS-DIGI.TTF')

computer_fond = pyglet.font.load("Computerfont")
ds_digi = pyglet.font.load("DS-DIGI")

main_window = pyglet.window.Window()
main_window.set_caption(PROJECT_NAME)

# Events
main_window.push_handlers(kb_handler.on_key_press)
main_window.push_handlers(kb_handler.on_key_release)

# KB config
key_mapper = KeymapConfigManager(kb_handler)

label = pyglet.text.Label("Hello World", x=main_window.width // 2, y=main_window.height // 2, font_name="DS-Digital",
                          font_size=36)
label.anchor_x = "center"

console = ui.ui.Console(translator)

source = pyglet.media.load('data/sounds/generated/en_signal_desc.wav')
source.play()

console.update()


@main_window.event
def on_draw():
    main_window.clear()
    console.draw()
    label.draw()


pyglet.app.run()