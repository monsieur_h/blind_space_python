from collections import deque
import pyglet


class Output(object):
    """
    An object that can output info to the player
    """

    def __init__(self, text_parser):
        """
        Ctor
        :TextParser text_parser: the parser required to translate the text_ids
        """
        self._parser = text_parser

    def _get_full_text(self, text_id_list):
        """
        Returns a list of text corresponding to the list of text id using its own parser
        :list text_id_list:
        :return:list
        """
        return [self._parser.get_text(text_id) for text_id in text_id_list]

    def output(self, text_id_list):
        """
        Outputs the string associated to text_id in an human-understandable format
        :string text_id:
        """
        pass


class TextToSpeechSpeaker(Output):
    """
    A class that handles the speech of the onboard AI. Searches for files, plays sounds
    """

    def __init__(self, text_parser):
        Output.__init__(self, text_parser)
        self._init_voice()

    def _say(self, text_id):
        # TODO: Find the file with the correct name and play it
        pass

    def _init_voice(self):
        # TODO: Read info from TextParser and store the folder to find mp3 in
        self._audio_folder = None

    def _get_audio_file(self, text_id):
        pass  # TODO: return the audio file name to be played

    def output(self, text_id_list):
        Output.output(self, text_id_list)
        for text_id in text_id_list:
            self._say(text_id)
            # pass  # TODO: Play the mp3 file


class AI(object):
    """
    Represents the AI of the player's ship
    """

    def __init__(self, text_parser):
        """
        Ctor
        :TextParser text_parser:The parser that will be used to find translations
        """
        self._speaker = TextToSpeechSpeaker()
        self._text_parser = text_parser

    def check_status(self, element):
        """
        Outputs the status of an element
        :param element:
        """
        pass


class GraphicalControlBoard(object):
    def __init__(self):
        self._controls = []
        self._displays = []

    def draw(self):
        for control in self._controls:
            control.draw()


class GraphicalControl(object):
    pass


class Console(GraphicalControl, Output):
    """
    Ouputs lines of text to the screen
    """
    DEFAULT_LINE_NUMBER = 5

    def __init__(self, text_parser):
        """
        
        :TextParser text_parser:
        """
        Output.__init__(self, text_parser)
        self._max_lines = Console.DEFAULT_LINE_NUMBER
        self._queue = deque(maxlen=self._max_lines)
        # TODO: Fix font management
        self._labels = [pyglet.text.Label("", font_name="DS-Digital") for i in range(self._max_lines)]
        self._x = 0
        self._y = 0

    def set_position(self, x, y):
        self._x = x
        self._y = y

    def update(self):
        self._set_labels_value()
        self._set_labels_position()
        self._set_labels_alpha()

    def _set_labels_alpha(self):
        for index in range(len(self._queue)):
            progression = float(index) / float(len(self._queue))
            alpha = int((1 - progression) * 255)
            self._labels[index].color = (255, 255, 255, alpha)

    def _set_labels_value(self):
        for index in range(len(self._queue)):
            self._labels[index].text = self._queue[index]

    def _set_labels_position(self):
        x = self._x
        y = self._y
        interline = 0

        for index in range(len(self._queue)):
            self._labels[index].x = x
            self._labels[index].y = y
            y += self._labels[index].content_height + interline

    def output(self, text_id_list):
        self.print_line(self._parser.get_text(text_id_list))

    def print_line(self, text):
        self._queue.append(text)

    def get_lines(self):
        return list(self._queue)

    def draw(self, batch=None):
        if batch:  # TODO:make it batch compatible
            pass
        else:
            for label in self._labels:
                label.draw()