class GameObject(object):
    '''
    Common base for all objects in the game
    '''
    def __init__(self):
        self._type = "unknown_object"
        self._description = "unknown_desc"
    
    def get_description(self):
        return self._description
    
    def get_type(self):
        return self._type
    
    
class Vector2(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y
        
    