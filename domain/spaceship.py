from domain.common import GameObject, Vector2


class SpaceObject(GameObject):
    def __init__(self):
        super(SpaceObject, self)
        self._position = Vector2(0, 0)

    def get_position(self):
        '''
        Returns a Vector2
        '''
        return self._position

    def set_position(self, position):
        '''
        Sets the position for the object
        :Vector2 position:
        '''
        self._position = position


class SpaceShip(SpaceObject):
    def __init__(self):
        super(SpaceShip, self).__init__()
        self._description = "spaceship_desc"
        self._type = "spaceship"

    def hyperspace_jump(self, coordinates):
        pass


class SpaceStation(SpaceObject):
    def __init__(self):
        super(SpaceStation, self).__init__()
        self._description = "spacestation_desc"
        self._type = "spacestation"