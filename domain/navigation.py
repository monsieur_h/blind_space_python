from domain.common import GameObject
from domain.spaceship import SpaceObject
from __builtin__ import super

class NavigableNode(GameObject):
    def __init__(self):
        super(NavigableNode, self).__init__()
        self._type = "navigationnode"
        self._description = "navigationnode_desc"
        
    def get_signal(self):
        '''
        Returns the signal of the node
        '''
        pass
    
    def get_adjacent_nodes(self):
        '''
        Returns a list of NavigableNodes
        '''
        pass

class Signal(SpaceObject):
    def __init__(self):
        super(Signal, self).__init__()
        self._type = "signal"
        self._description = "signal_desc"
