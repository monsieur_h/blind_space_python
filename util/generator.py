import os
import re
import GoogleTTS
from multiprocessing.dummy import Pool as ThreadPool
from pydub import AudioSegment


class AudioGenerator(object):
    pass


class GoogleTTSGenerator(AudioGenerator):
    """
    Creates sound for any spoken text in the program
    """

    def __init__(self):
        self._parser = None

    def generate_sound_threaded(self, param_dict=None):
        """
        Wrapper function to be called in a map for threading
        :type param_dict: dict
        """
        if not param_dict:
            return
        if param_dict["filename"] and param_dict["lang"] and param_dict["text"]:
            self.generate_sound(param_dict["filename"], param_dict["lang"], param_dict["text"])

    def generate_sound(self, dest, lang, text):
        """
        Creates a mp3 sound to be reused
        :string filename:Name of the file to be created
        :string lang:two letter representation of the language
        :string text:the content to be read in the mp3 file
        """
        language = lang.lower()
        print "Creating {}".format(dest)
        GoogleTTS.audio_extract(text, {"language": language, "output": dest})


class AudiofileManager(object):
    """
    Checks if mp3 text files are missing and builds them if needed.
    Uses thread to parrallelize mp3 creation
    """

    RE_VALID_KEY_FORMAT = re.compile("[a-z-09_]+")
    RE_AUDIO_FILENAME_FORMAT = re.compile("[a-z]{2}_[a-z0-9_]+\.[a-z0-9]{2,4}")
    FILENAME_FORMAT = "{}_{}.{ext}"
    POOL_NUMBER = 4

    def __init__(self, locale, output_folder, file_ext="mp3"):
        self._locale = locale.lower()
        self._output_folder = os.path.abspath(output_folder)
        self._thread_pool = ThreadPool(self.POOL_NUMBER)
        self._generator = None
        self._text_parser = None
        self._key_list = None
        self._file_ext = file_ext
        self._test_path()

    def _test_path(self):
        """
        Tests and create the folder to store generated audio files in
        """
        if os.path.isdir(self._output_folder):
            return True
        else:
            if os.path.isdir(os.path.basename(self._output_folder)):
                print "Creating {}...".format(self._output_folder)
                os.mkdir(self._output_folder)

    def set_generator(self, generator):
        """
        Generator setter (needed to build sounds)
        :Generator generator:
        """
        self._generator = generator

    def set_parser(self, parser):
        """
        Parser setter (needed to build sounds)
        :type parser: TextParser
        """
        self._parser = parser

    def _get_audio_files(self):
        """
        Returns a list of already present audiofiles
        """
        return [os.path.join(self._output_folder, filename) for filename in os.listdir(self._output_folder) if
                self.RE_AUDIO_FILENAME_FORMAT.match(filename)]

    def _get_missing_files(self):
        """
        Returns a list of audio files to generate, based on the locale and the files present at self._output_folder
        """
        present_files = self._get_audio_files()
        needed_files = [self._make_filename(key) for key in self._key_list if
                        self._make_filename(key) not in present_files]
        return needed_files

    def _get_missing_keys(self):
        """
        Returns a list of keys that needs to be translated as audio files
        """
        audio_files = [os.path.basename(name) for name in self._get_audio_files()]
        needed_keys = [key for key in self._key_list if self._make_filename(key) not in audio_files]
        return needed_keys

    def _make_filename(self, key):
        """
        Returns a filename give a key based on the current locale
        :string key:
        """
        return self.FILENAME_FORMAT.format(self._locale, key, ext="mp3")

    def _make_full_filename(self, key):
        """
        Returns an absolute filename (path + name)
        :string key:
        """
        return os.path.join(self._output_folder, self._make_filename(key))

    def set_key_list(self, key_list):
        """
        Sets the key list to be generated
        :list key_list:a list of string keys (format [a-z0-9_]+ compliant)
        """
        self._key_list = [key_str for key_str in key_list if self.RE_VALID_KEY_FORMAT.match(key_str)]

    def build_audio_files(self):
        """
        Build audio files using POOL_NUMBER threads with a text parser and a generator
        """
        if not self._key_list or not self._generator or not self._parser:
            return
        args_list = []
        print "Found {} missing audio keys...".format(len(self._get_missing_keys()))
        for key in self._get_missing_keys():
            args_list.append({"filename": self._make_full_filename(key), "lang": self._locale,
                              "text": self._parser.get_text(key).encode('utf-8')})

        self._thread_pool.map(self._generator.generate_sound_threaded, args_list)

    def convert_audio_files(self, source_format='mp3', dest_format='wav'):
        """
        Converts all audio files to a different encoding using pydub
        :param source_format: a 3-4 letters extension representing the source format
        :type source_format: str
        :param dest_format: a 3-4 letters extension representing the target format
        :type dest_format: str
        """
        all_audio_files = self._get_audio_files()
        # List of files without their extension for comparison
        source_files = [os.path.splitext(filename)[0] for filename in all_audio_files if filename.endswith(source_format)]
        dest_files = [os.path.splitext(filename)[0] for filename in all_audio_files if filename.endswith(dest_format)]
        to_convert = [filename for filename in source_files if filename not in dest_files]
        for filename in to_convert:
            abs_src_path = os.path.abspath(os.path.join(self._output_folder, "{}.{}".format(filename, source_format)))
            abs_dst_path = os.path.abspath(os.path.join(self._output_folder, "{}.{}".format(filename, dest_format)))
            sound_sample = AudioSegment.from_file(abs_src_path, format=source_format)
            sound_sample.export(abs_dst_path, dest_format)
            print "Done converting {} from {} to {}".format(filename, source_format, dest_format)
