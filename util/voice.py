import re

class VoiceGenerator(object):
    '''
    Creates voice files from a given text
    '''
    RE_FILENAME = re.compile("[a-z]{2}_[a-z]+\.[a-z0-9]")
    
    def __init__(self, voice):
        self._init_voice(voice)
        
    def _init_voice(self, voice):
        self._voice_name = voice
        
    def _generate_file(self, text, output):
        pass#TODO: TTS from whatever it is