import json
import os

class DuplicateTextKeyException(Exception):
    pass

class MissingTranslationException(Exception):
    pass

class TextParser(object):
    '''
    Retrieves text from the filesystem
    '''
    def __init__(self, path, lang="en"):
        '''
        Ctor
        :string path:path to i18n files
        :string lang: two characters representation of the language (en, fr, es...)
        '''
        self._file_path = path
        self.set_lang(lang)
        
    def _load_text_files(self):
        '''
        Loads all text files into the objects
        '''
        self._text_files = [ filename for filename in os.listdir(self._file_path) if filename.endswith('.json') ]
        self._text = {}
        for filename in self._text_files:
            file_content = json.load(open(os.path.join(self._file_path,filename),'r'))
            if not self._lang in file_content:
                raise MissingTranslationException("File {} has no translation for '{}'".format(filename, self._lang)) 
            locale_content = file_content[self._lang]
            for key in locale_content:
                if key not in self._text:
                    self._text[key] = locale_content[key]
                else:
                    raise DuplicateTextKeyException("{} key already exists".format(key))
               
    def set_lang(self, lang="en"):
        '''
        Changes the lang and reloads accordingly
        :string lang: two characters representation of the language (en, fr, es...)
        '''
        self._lang = lang
        self._load_text_files()
        
    def get_text(self, key):
        '''
        Returns the translated text for a given key, or the key
        :string key:The string key representing the translation
        '''
        if key in self._text:
            return self._text[key]
        else:
            #TODO: Throw an exception?
            return key
        
    def get_keys(self):
        '''
        Returns the list of keys known by the object
        '''
        return self._text.keys()
        