class Command(object):
    '''
    An object to implement the Command Pattern
    '''

    def execute(self, target=None):
        self._target = target

    def undo(self):
        pass

    def redo(self):
        pass


class HyperSpaceJumpCommand(Command):
    '''
    A command that makes a ship jump to coordinates throught hyperspace
    '''

    def __init__(self, target_coordinates):
        self._coordinates = target_coordinates

    def execute(self, target=None):
        Command.execute(self, target=target)
        self._target.hyperspace_jump(self._coordinates)


class StatusRequestCommand(Command):
    def __init__(self, request):
        self._request = request

    def execute(self, target=None):
        Command.execute(self, target=target)
        print 'Executing command {}'.format(self._request)
        # self._target.check_status(self._request)


