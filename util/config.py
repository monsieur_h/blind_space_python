import pyglet
import platform
import sys

sys.path.append('..')
import util.controls
import util.commands



class ConfigManager(object):
    SYS_LINUX = 1
    SYS_WINDOWS = 2
    SYS_OSX = 3

    def __init__(self):
        self._platform = self._get_platform()
        self._set_audio_driver_priority(self._platform)

    def _get_platform(self):
        current_system = platform.system()
        if current_system == "Windows":
            return self.SYS_WINDOWS
        elif current_system == "Linux":
            return self.SYS_LINUX
        elif current_system == "MacOS":
            return self.SYS_OSX

    def check_audio_drivers(self):
        if "openal" in pyglet.options["audio"]:
            return True
        else:
            return False

    def _set_audio_driver_priority(self, platform):
        if platform == self.SYS_LINUX:
            pyglet.options["audio"] = ("openal", "alsa", "silent")
        elif platform == self.SYS_WINDOWS:
            pyglet.options["audio"] = ("openal", "directsound", "silent")
        elif platform == self.SYS_OSX:
            pyglet.options["audio"] = ("openal", "silent")


class KeymapConfigManager(object):
    def __init__(self, keyboard_dispatcher):
        """
        :param keyboard_dispatcher the keyboard dispatcher to configure
        :type keyboard_dispatcher: KeyboardDispatcher
        """
        self._dispatcher = keyboard_dispatcher
        self._actions = {}
        self._read_config()
        self._set_config()

    def _read_config(self):
        pass  # TODO read from file

    def _set_config(self):
        """
        Forwards the config to the keyboard dispatcher
        """
        # TODO: remove this once the file reading is done
        my_command = util.commands.StatusRequestCommand("Test")
        dir(self._dispatcher)
        self._dispatcher.set_handler_by_key('A', my_command)

    def _save_config(self):
        pass  # TODO: save to file

