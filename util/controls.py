import pyglet.window.key as keyboard_const
import commands


class KeyboardDispatcher(object):
    """
    Dispatch the keypresses to objects using commands
    """

    def __init__(self):
        self._init_consts()
        self._init_commands()

    def _init_consts(self):
        self._consts = keyboard_const._key_names

    def _init_commands(self):
        self._commands = {}  # TODO: Dict comprehension
        for symbol in self._consts:
            self._commands[symbol] = commands.Command()

    def _get_key_name(self, key):
        """
        :param key: A number representing the constant of the key
        :type int
        :return: A string representing the name of the key
        :rtype str
        """
        if key in self._consts:
            return self._consts[key]

        return "Unknown"

    def on_key_press(self, symbol, modifiers):
        # print "Pressed : {}".format(symbol)
        print "Pressed :\t{} ({})".format(self._get_key_name(symbol), symbol)

    def on_key_release(self, symbol, modifiers):
        print "Released :\t{} ({})".format(self._get_key_name(symbol), symbol)
        if symbol in self._commands:
            self._commands[symbol].execute()


    def _get_symbol(self, key_name):
        """
        Returns the symbol for a key name
        :param key_name: Name of the key
        :type str
        :return:the constant corresponding to that key
        :rtype int/None
        """
        if key_name in self._consts.values():
            for symbol, name in self._consts.iteritems():
                if name == key_name:
                    return symbol

        return None


    def set_handler(self, symbol, command):
        self._commands[symbol] = command


    def set_handler_by_key(self, key, command):
        """
        :param key: The keyname to attach to
        :type key: str
        :param command: The command that will be run when the key is released
        :type command: commands.Command
        """
        self._commands[self._get_symbol(key)] = command

